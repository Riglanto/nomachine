// Library headers
#include <GL/glew.h> // include GLEW and new version of GL on Windows
#include <GLFW/glfw3.h> // GLFW helper library

// Cg includes
#include <Cg/cg.h>
#include <Cg/cgGL.h>

#include <iostream>
#include <fstream>

using namespace std;

CGprofile pixelShaderProfile, vertexShaderProfile;
CGcontext context;
CGprogram pixelShader, vertexShader;
int screenHeight, screenWidth;

static GLuint loadBMP(const char* filepath);
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
static void buildQuad(float scale);
static void render(GLFWwindow* window);
static GLuint loadBMP(const char* filepath);
void initCG();
GLFWwindow * initOpenGL();