/*
* Radoslaw Iglantowicz
* for NoMachine
*/
#include "nomachine.h"

int main(int argc, char* argv[]) {
	GLFWwindow* window = initOpenGL();
	initCG();

	render(window);

	cgDestroyContext(context);
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

static void buildQuad(float scale)
{
	scale = 1 - 2 / scale;
	glBegin(GL_QUADS);
		glTexCoord2d(0.0, 0.0);
		glVertex3f(scale, scale, 0.f);

		glTexCoord2d(1.f, 0.0);
		glVertex3f(scale, 1.f, 0.f);

		glTexCoord2d(1.f, 1.f);
		glVertex3f(1.f, 1.f, 0.f);

		glTexCoord2d(0.0, 1.f);
		glVertex3f(1.f, scale, 0.f);
	glEnd();
}

static void render(GLFWwindow* window)
{
	double x, y;
	CGparameter mousePosX, decal = nullptr;
	cgGLEnableProfile(vertexShaderProfile);
	cgGLBindProgram(vertexShader);
	cgGLEnableProfile(pixelShaderProfile);
	cgGLBindProgram(pixelShader);

	GLuint tex = loadBMP("MARBLES1024.BMP");

	cgGLSetTextureParameter(decal, tex);

	while (!glfwWindowShouldClose(window))
	{
		//update mouse position
		glfwGetCursorPos(window, &x, &y);
		mousePosX = cgGetNamedParameter(pixelShader, "mousePosX");
			
		cgSetParameter1d(mousePosX, x / screenWidth); 
		buildQuad(1);
		cgSetParameter1d(mousePosX, 1);
		buildQuad(5);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	cgGLDisableProfile(pixelShaderProfile);
	cgGLDisableProfile(vertexShaderProfile);
	glDeleteTextures(1, &tex);
}

static GLuint loadBMP(const char* filepath) {
	errno_t err = 0;
	FILE * file;
	err = fopen_s(&file, filepath, "r");
	if (err == 0)
	{
		cout << "OK! Image was opened";
	}
	else
	{
		cout << "Image could not be opened - error: " << err;
		exit(EXIT_FAILURE);
	}

	unsigned char header[54];
	fread_s(header, 54, 54, 1, file);
	if (header[0] != 'B' || header[1] != 'M') {
		cout << "Image is not .bmp";
		exit(EXIT_FAILURE);
	}

	int height, width, size;
	height = *(int*)&header[22];
	width = *(int*)&header[18];
	if (height != width) {
		cout << "Texture should have height equal to width";
		exit(EXIT_FAILURE);
	}
	size = 3 * height * width;

	unsigned char *data = new unsigned char[size];
	fread_s(data, size, size, 1, file);

	fclose(file);

	// Create one OpenGL texture
	GLuint texture;

	// allocate a texture name
	glGenTextures(1, &texture);

	// select our current texture
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);
	return texture;
}

void initCG() {
	context = cgCreateContext();
	if (context == nullptr) exit(EXIT_FAILURE);

	vertexShaderProfile = cgGLGetLatestProfile(CG_GL_VERTEX);
	if (vertexShaderProfile == CG_PROFILE_UNKNOWN) exit(EXIT_FAILURE);
	cgGLSetOptimalOptions(vertexShaderProfile);

	pixelShaderProfile = cgGLGetLatestProfile(CG_GL_FRAGMENT);
	if (pixelShaderProfile == CG_PROFILE_UNKNOWN) exit(EXIT_FAILURE);
	cgGLSetOptimalOptions(pixelShaderProfile);

	vertexShader = cgCreateProgramFromFile(context, CG_SOURCE,
		"vertexShader.cg", vertexShaderProfile, "main", 0);
	pixelShader = cgCreateProgramFromFile(context,CG_SOURCE,
		"pixelShader.cg",pixelShaderProfile,"main",0);
	if (vertexShader == nullptr || pixelShader == nullptr) exit(EXIT_FAILURE);
	cgGLLoadProgram(vertexShader);
	cgGLLoadProgram(pixelShader);

	cgGLRegisterStates(context);
	cgGLSetManageTextureParameters(context, CG_TRUE);
}

GLFWwindow * initOpenGL() { 
	// start GL context and O/S window using the GLFW helper library
	if (!glfwInit()) {
		cout << "ERROR: could not start GLFW3";
		exit(EXIT_FAILURE);
	}

	// detect resolution
	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	screenHeight = mode->height;
	screenWidth = mode->width;
	GLFWwindow * window = glfwCreateWindow(screenWidth, screenHeight, "Nomachine", glfwGetPrimaryMonitor(), nullptr);
	if (!window) {
		cout << "ERROR: could not open window with GLFW3";
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);

	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();
	return window;
}